import React, { Component } from "react";
import { Text, View, ScrollView, TouchableOpacity, StyleSheet, Image } from "react-native";

class List extends Component {
    state = {
        key: 0
    }
  render() {
    return (
      <ScrollView>
          <View style={{padding: 5}}>
        {this.props.elements.map((item, i) => (
          <TouchableOpacity
            key={i}
            style={styles.container}
            onPress={() => this.props.alertItemName(i)}
          >
            <Text style={styles.text}>Имя: {item.name} </Text>
            <Text style={styles.text}>Пол: {item.gender == 'female' ? 'Женский' : 'Мужской'} </Text>
            <Text style={styles.text}>Страна: {item.region} </Text>
            <Text style={styles.text}>Дата рождения: {item.birthday} </Text>
            <Text style={styles.text}>Email: {item.email} </Text>
            <Image
              source={{
                uri:
                  item.photo
              }}
              style={{ width: 200, height: 200 }}
            />
          </TouchableOpacity>
        ))}
          </View>
      </ScrollView>
    );
  }
}
export default List;

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    paddingBottom: 5,
    borderColor: 'gray',
    borderWidth: 2,
    backgroundColor: "white",
    alignItems: "center"
  },
  text: {
    color: "#4f603c"
  }
});
