import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import { NavigationComponent } from "../Navigator/NavigationComponent";

export class HexCom extends Component {
  state = {
    color: '',
    styleColor: '#fff'
  }
  changeColor = () => {
    this.setState({ styleColor: '#'+this.state.color })

  }
  inputColor = (color) => {
      this.setState({color: color})
  }
  render(){
  return (
    <View style={styles.container}>
      <NavigationComponent navigation={this.props.navigation} />
      <View style={styles.inner}>
        <TextInput
        style={{fontSize: 30, marginBottom: '5%', color: 'yellow', textAlign: 'center', textDecorationLine: "none"}}
        underlineColorAndroid = 'black'
        placeholder = "Enter hex"
        placeholderTextColor = "#9a73ef"
        maxLength = {6}
        onChangeText = {this.inputColor}
        value={this.state.color}/>

        <TouchableOpacity
        style={{borderColor: 'white', borderWidth: 2}}
        onPress = {
          () => this.changeColor()
          }
        >
          <Text style={{fontSize: 30, color: 'white', marginBottom: '2%', padding: 10}}>Do</Text>
        </TouchableOpacity>

        <View style = {[styles.element,{backgroundColor: this.state.styleColor, marginTop: '5%'}]}></View>
      

      </View>

    </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    width: "100%",
    height: "100%"
  },
  inner: {
    marginTop: '5%',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: "50%"
  },
  element: {
    flex: 1,
    width: "50%",
    height: "20%"
  }  
});
