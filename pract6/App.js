import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import RoutingPage from './Navigator/Navigator';

export default function App() {
  return (
    <RoutingPage />
  );
}