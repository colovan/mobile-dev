import { ListCom } from '../ListCom/ListCom';
import { createStackNavigator } from 'react-navigation-stack';
import { FigureCom } from '../FigureCom/FigureCom';
import { HexCom } from '../HexCom/HexCom';
import { createAppContainer } from 'react-navigation';

const navigator = createStackNavigator(
  {
    List: {
      screen: ListCom
    },
    Figures: {
      screen: FigureCom
    },
    Hex: {
      screen: HexCom
    }
  },
  {
    initialRouteName: 'List'
  }
);

const RoutingPage = createAppContainer(navigator);

export default RoutingPage;