import React, { Component } from "react";
import { View, StyleSheet, Button } from "react-native";

export class NavigationComponent extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    return <View style={styles.buttons}>
      <Button title="List" onPress={ () => this.props.navigation.navigate('List') }></Button>
      <Button title="Figures" onPress={ () => this.props.navigation.navigate('Figures') }></Button>
      <Button title="Hex" onPress={ () => this.props.navigation.navigate('Hex') }></Button>
    </View>
  }
}

const styles = StyleSheet.create({
  buttons: {
    marginTop: 15,
    flexDirection: "row",
    marginLeft: '25%',
    width: '50%',
  }
});
