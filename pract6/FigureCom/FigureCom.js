import React, { Component } from "react";
import { View, StyleSheet, Text, Image, ScrollView } from "react-native";
import { NavigationComponent } from "../Navigator/NavigationComponent";

export class FigureCom extends Component {
  render() {
    return (
      <View>
        <NavigationComponent navigation={this.props.navigation} />
        <View style={styles.container}>
          <View style={styles.redbox} />
          <View style={styles.bluebox} />
          <View style={styles.blackbox} />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "grey",
    height: 600
  },
  redbox: {
    width: 100,
    height: 100,
    backgroundColor: "red"
  },
  bluebox: {
    width: 100,
    height: 100,
    backgroundColor: "blue"
  },
  blackbox: {
    width: 100,
    height: 100,
    backgroundColor: "black"
  }
});
